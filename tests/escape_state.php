<?php

namespace Tests;

require __DIR__ . '/../app/utils/escape_state.php';
use \Exception as Exception;

function escapeStateTest() {

    (function () {
        $state = array(
            'tag' => "<script>console.log('hello');</script>",
            'sub' => array(
                'tag' => '<script>console.log("sub");</script>'
            )
        );

        $state = \App\Utils\escapeState($state);

        if ($state['tag'] !== '&lt;script&gt;console.log(&#039;hello&#039;);&lt;/script&gt;') {
            throw new Exception('Single quote XSS not escaped');
        }

        if ($state['sub']['tag'] !== '&lt;script&gt;console.log(&quot;sub&quot;);&lt;/script&gt;') {
            throw new Exception('Nested double quote XSS not escaped');
        }

    })();
}

escapeStateTest();

?>
