<?php

namespace Tests;

require __DIR__ . '/../app/model.php';
use \Exception as Exception;

/*
 * There are testing frameworks for PHP.
 *
 * However, I 100% know that I'm not going to need something complicated for
 * this app. And given that this is a small project, and given that I'm on a
 * deadline, putting together a three or four line bash script is better than
 * researching one of those frameworks.
 *
 */
function modelTest() {
    $dbPath = __DIR__ . '/../db/modelTest.db';

    if (is_writable($dbPath)) {
        unlink($dbPath);
    }

    $model = new \App\Model($dbPath);

    /* Create user */
    (function ($model) {
        $result = $model->createUser('dan', 'abc');

        if (!$result) {
            throw new Exception('Creating new user failed');
        }
    })($model);

    /* Don't allow creating twice though */
    (function ($model) {
        $result = $model->createUser('dan', 'cba');

        if ($result) {
            throw new Exception('Creating duplicate user succeeded');
        }
    })($model);

    /* User ID lookup */
    (function ($model) {
        $id = $model->getUserId('dan');
        $result = $model->getUser($id)[0];

        if ($result !== 'dan') {
            throw new Exception('User lookup failed');
        }
    })($model);

    /* User password comparison */
    (function ($model) {
        $id = $model->getUserId('dan');
        $password = $model->getUser($id)[1];

        $result = password_verify('abc', $password);

        if (!$result) {
            throw new Exception('Password comparison failed');
        }
    })($model);

    /* Creating notes */
    /* Retrieving notes attached to users */
    /* Retrieving note text */
}

modelTest();

?>
