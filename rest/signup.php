<?php
require __DIR__ .'/../app/controller.php';

(function () {
    header('Content-Type: application/json;charset=utf-8');
    session_start();

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        return;
    }

    /* Work with JSON instead of form data */
    $_POST = json_decode(file_get_contents('php://input'), true);

    $dbPath = __DIR__ . '/../db/main.db';
    $controller = new \App\Controller($dbPath);

    /* Reset everything if already logged in */
    if ($_SESSION['loggedin']) {
        $controller->logout();

        session_unset();
        session_destroy();
        session_start(); /* not sure if this is necessary */
    }

    $username = $_POST['username'];
    $password = $_POST['password'];
    $result = $controller->createUser($username, $password);

    /* Auto-login the user if they've just created an account */
    if ($result) {
        $id = $controller->login($username, $password);
        $_SESSION['userid'] = $id;
        $_SESSION['loggedin'] = true;
    }

    echo json_encode(array(
        'result' => $result
    ));
})();

?>
