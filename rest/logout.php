<?php

require __DIR__ . '/../app/controller.php';

(function () {
    header('Content-Type: application/json');
    session_start();

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        return;
    }

    $_POST = json_decode(file_get_contents('php://input'), true);

    $dbPath = __DIR__ . '/../db/main.db';
    $controller = new \App\Controller($dbPath);

    $result = $controller->logout();
    if ($result) {
        session_unset();
        session_destroy();
    }

    echo json_encode(array(
        'result' => $result
    ));
})();

?>
