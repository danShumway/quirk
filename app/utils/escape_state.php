<?php

namespace App\Utils;

/*
 * Security best practices that I hold to:
 *
 * A) Do all of your escaping for an output format in one place. Ideally, this
 * would be built into the templates themselves so there was no way I could
 * forget to do it.
 *
 * B) Escape on output, not input. It's difficult to escape input for multiple
 * formats at the same time. For example, input that's escaped for SQL needs to
 * be handled differently than input that's escaped for HTML. Escape when you're
 * ready to use data, not before.
 *
 * This also has the advantage of allowing you to know for sure that all of your
 * data will be safe no matter where it comes from. If you're escaping input,
 * it's difficult to evaluate whether or not a template is secure unless you
 * evaluate the entire program. If you escape on output, you can evaluate just
 * one function.
 *
 */
function escapeState($state) {

    /*
     * TODO: I ended up doing something weird with references here. I haven't
     * sat down yet to figure out exactly how PHP handles references. But, I'm
     * pretty sure this is wrong. Escaping state should return a new state. I
     * should be treating state like it's immutable.
     */
	array_walk($state, function (&$value) {
		if (is_string($value)) {
            $value = htmlspecialchars($value, ENT_QUOTES);
		} elseif (is_array($value)) {
			$value = escapeState($value);
		} elseif (is_numeric($value) || $value == null) {
			//No operation necessary
		} else {
			//Don't attach non-arrays
			//instances/functions shouldn't be on $state anyway.
			throw new \Exception('Template state must not contain functions or instances.');
		}

	});

	return $state;
};

?>
