<?php

namespace App;

class Model {

    private $pdo; //database connection
    function __construct ($path) {

        /*
         * There's theoretically some extra error checking that could go here.
         * In practice, I think PHP will already throw exceptions if something
         * goes wrong. And if you try to connect to a bad database, I want to
         * just fail fast.
         *
         * I guess the danger is that the error gets reflected into your
         * template? Will need to check how PHP handles that by default.
         */
        $this->pdo = new \PDO('sqlite:' . $path);

        /*
         * Initialize database if it hasn't been handled yet. You should
         * probably avoid re-running these on every request.
         *
         * It's just stupid unnecessary overhead. BUT... it's super quick, and
         * it means that there's no extra provisioning step... Good enough for
         * now? Good enough for now.
         */
        $this->pdo->exec('CREATE TABLE IF NOT EXISTS users (
                             user_id INTEGER PRIMARY KEY,
                             user_name TEXT NOT NULL,
                             user_password TEXT NOT NULL)');

        $this->pdo->exec('CREATE TABLE IF NOT EXISTS notes (
                             note_id INTEGER PRIMARY KEY,
                             note_user INTEGER NOT NULL
                             note_text TEXT)');
    }

    function getUser ($id) {
        $query = $this->pdo->prepare('SELECT user_name, user_password FROM users WHERE user_id = :id');

        /*
         * ``prepare`` can just return false sometimes. This is something I
         * should check for, I guess, but... honestly I don't see the point.
         *
         * A boolean is not enough information for me to fail gracefully. I
         * won't know why it failed, or... anything, really.
         *
         * Might as well just throw an exception, and might as well let that
         * exception be PHP's.
         */
        $query->bindParam(':id', $id, \PDO::PARAM_INT);
        $query->execute();

        return $query->fetch();
    }

    function getUserId($name) {
        $query = $this->pdo->prepare('SELECT user_id FROM users WHERE user_name = :name');
        $query->bindParam(':name', $name, \PDO::PARAM_STR);
        $query->execute();

        $result = $query->fetch();
        return $result !== false ? $result[0] : false;
    }

    function getUserNotes ($id) {}

    //function deleteUser($id) {}

    function createUser($name, $password) {
        $query = $this->pdo->prepare('INSERT INTO users (user_name, user_password)
                                        VALUES (:name, :password)');

        /* Can't have two users with the same name, since that's your login */
        /* TODO: I know that PHP often just returns false for errors, but I want to do better */
        if ($this->getUserId($name) !== false) { return false; }

        /*
         * Always hash and salt passwords before storing them.
         *
         * TODO: how does PHP choose the salt? Is it based on the password
         * itself? Why don't I need to store it? This isn't how I thought
         * password salts worked.
         *
         * See http://php.net/manual/en/function.password-hash.php but on first
         * glance this looks crazy. What's the point of storing the salt *on*
         * the password? Doesn't that just invalidate all of the security gains?
         */
        $password = password_hash($password, PASSWORD_DEFAULT);

        $query->bindParam(':name', $name, \PDO::PARAM_STR);
        $query->bindParam(':password', $password, \PDO::PARAM_STR);

        return $query->execute();
    }

    function getNote ($id) {}

    //function deleteNote ($id) {}

    function createNote () {}
}

?>
