<?php

namespace App\Templates;

function headerTemplate () { ?>

    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" >

    <title>Quirk</title>
    <meta name="description" content="An unreliable notetaking app">

    <link href="/styles/main.css">
    </head>

<?php
}
?>
