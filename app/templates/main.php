<?php


namespace App\Templates;
require_once __DIR__ . '/../utils/escape_state.php';
require_once __DIR__ . '/header.php';

function mainTemplate ($state) {
    $state = \App\Utils\escapeState($state); ?>

    <!DOCTYPE html>
    <html lang="en-US">
    <?php headerTemplate(); ?>

    <body>
    <main class="app">

      <header>
        <p>Logged in as <?php echo $state['username']; ?>.</p>
        <button class="logout__logout">Logout</button>
      </header>

    </main>

    <script src="/scripts/logout.js"></script>
    </body>
    </html>


<?php
}
?>
