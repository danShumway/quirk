<?php

namespace App\Templates;
require_once __DIR__ . '/../utils/escape_state.php';
require_once __DIR__ . '/header.php';

function loginTemplate ($state) {
    $state = \App\Utils\escapeState($state); ?>

    <!DOCTYPE html>
    <html lang="en-US">
    <?php headerTemplate(); ?>

    <body>
    <main class="login">

      <form class="login__login"
            action="/rest/login.php"
            method="post">

        <h1>Login</h1>

        <label for="login__login__name">Username: </label>
        <input id="login__login__name"
               type="text"
               name="username"
               value=""
               required>
        
        <label for="login__login__password">Password: </label>
        <input id="login__login__password"
               type="password"
               name="password"
               value=""
               required>

        <input type="submit"
               value="login">

      </form>


      <form class="login__signup"
            method="post"
            action="/rest/signup.php">
        
        <h1>Create New Account</h1>

        <label for="login__signup__name">Username: </label>
        <input id="login__signup__name"
               type="text"
               name="username"
               value=""
               required>
        
        <label for="login__signup__password">Password: </label>
        <input id="login__signup__password"
               type="password"
               name="password"
               value=""
               required>

        <input type="submit"
               value="create">

      </form>
    </main>

    <script src="/scripts/login.js"></script>
    </body>
    </html>

<?php
}
?>
