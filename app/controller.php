<?php

namespace App;
require __DIR__ . '/../app/model.php';

/*
 * In PHP, state isn't long lived. The philosophy is that at the end of every
 * request we throw everything away that isn't part of the session. I'm still
 * kind of adapting to this, but the basic upshot that I'm seeing is that I need
 * to preserve a lot less state.
 *
 * It kinda makes me wonder what the point even is of making this into a class.
 * Maybe you don't really need extra methods like ``logout``.
 */
class Controller {

    private $model;
    function __construct ($path) {
        $this->model = new Model($path);
    }

    function login($username, $password) {

        if(!$this->userExists($username)) {
            return false;
        }

        $id = $this->model->getUserId($username);
        $passwordHash = $this->model->getUser($id)[1];
        if (!password_verify($password, $passwordHash)) {
            return false;
        }

        return $id;
    }

    /* This is entirely handled by PHP's built in session. */
    function logout() {
        return true;
    }

    function userExists($username) {
        $id = $this->model->getUserId($username);
        return $id !== false;
    }

    function createUser($username, $password) {
        return $this->model->createUser($username, $password);
    }

    function getUser($id) {
        return $this->model->getUser($id);
    }

    /* Return note titles and ids */
    function getNotes($id) {
        
    }

    function getNote($userId, $id) {
        //Check to make sure that note belongs to user.
    }

}

?>
