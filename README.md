# Quirk

An unreliable notetaking app. In progress.

## What is Quirk?

Quirk is intended to be a joke notetaking app. It will function like a normal
notetaking app, with the addition of several odd behaviors.

 - On saving a note, all numbers will be auto-incremented by 1.
 - Various words will be switched. For example, all instances of the word
   "giraffe" will be replaced with "elephant".
 
## But why?

[Thanks for taking the time to read this blog
post.](https://mango.pdf.zone/stealing-chrome-cookies-without-a-password#hey-so-how-did-you-end-up-finding-this-like-why-did-you-need-to-get-someones-chrome-cookies-in-the-first-place)

In actuality I just wanted a small, relatively inoffensive app I could use to
learn PHP, notetaking apps are a decent way to do that, and making something
that (to me) is funny makes that process more interesting and lighthearted.

I think that joke apps are often some of the best ways to play around with
technologies that you're not familiar with, because it takes away all of the
stress of design. Designing a good app is hard work by itself, even before you
start fighting with unfamiliar tools. So if you have unfamiliar tools, maybe
don't worry about designing a good app?

## What does the architecture look like?

Quirk is a pretty simple SPA.

Your entry point is ``index.php``, and the REST API is defined under ``rest``.

When you enter ``index.php`` I check to see if you're logged in and serve you
either a login template or the app itself accordingly.

Quirk uses a Model/View/Controller setup. I initialize a Controller, which
initializes a Model, and I use that to render out the initial page. Then,
everything else is handled via clientside REST requests. Each REST request is a
wrapper around some kind of Controller method.

Data is stored in Sqlite, because it's the simplest database that I know of.
Rest requests expect and return JSON. Assets are served out of ``scripts`` and
``styles``.

Unit tests are stored in the ``tests`` folder. The test runner is ``test.sh``.
I'm not using a testing framework, because for an app this small I don't really
see a need for one. Code coverage is not *great*, but not completely terrible
either. At some point I might add a few integration tests as well using
something like [Webdriver](https://www.w3.org/TR/webdriver1/).

## Any dependencies?

PHP's Sqlite driver, which I believe needs to be installed via your own OS. As
of >PHP 5.4, this is no longer auto-bundled. There's a Composer file, but as you
can see I'm not currently using any packages, so you don't really *need*
Composer at the moment.

Deployment should be done through... Apache, I guess? Or something equivalent. I
haven't really gotten that far yet. I don't think I'm using anything that
requires greater than around PHP 5.4, but I've only tested on PHP 7.

You can also deploy using PHP's built in server, ``php -S localhost:8000``,
although note that this is probably much less secure, because I suspect it
doesn't set CORS headers correctly, and it probably doesn't support SSL.

## What security measures does/will Quirk have?

Your standard stuff:

- Passwords are salted and hashed rather than being stored in plain text.
- SQL/XSS attacks are blocked.
- Notes will be at-rest encrypted.
- CORS headers will be properly set to keep session data safe.

Quirk will not be zero-knowledge. This is partially because word replacements
happen serverside, and I need access to note text to do that. This is also
because the point of Quirk is for me to learn PHP, so running all of my logic
locally would be, well, counterproductive.

Because Quirk is not zero knowledge, you should never store anything in it that
you are not comfortable with your admin seeing. 'Cause they can.

I have not currently added any mechanisms like rate limiting to slow down bots.
I may or may not in the future. But if I do, it won't be a Google Captcha.

## Why is it important for a joke app to have decent security?

[Thanks for taking the time to read this blog
post.](https://mango.pdf.zone/stealing-chrome-cookies-without-a-password#hey-so-how-did-you-end-up-finding-this-like-why-did-you-need-to-get-someones-chrome-cookies-in-the-first-place)

## Is this hosted anywhere? What's your privacy policy?

This will probably be hosted somewhere in the future, at least temporarily.

Do not store sensitive data on my servers. Do not store nasty or illegal data on
my servers. I don't want to know your name or your email, I made logins
anonymous and email free on purpose.

Assume that I can read anything you post. I'm probably not going to, because I
don't care about you. But I could. If someone's username looks weird, or if I
decide to re-architecture something, or even if I get bored, I may clear out the
database at any time and delete all of your data.

All of life is transient and temporary, so in Quirk, your data is too.

I don't have a GDPR policy in place, so if you're in the EU, don't use this I
guess? But seriously, if you've read this far, what's wrong with you? Why would
you store personal information on this anyway?

## What's wrong with *me*? Why does *your* unfinished joke app have a privacy policy?

[Thanks for taking the time to read this blog
post.](https://mango.pdf.zone/stealing-chrome-cookies-without-a-password#hey-so-how-did-you-end-up-finding-this-like-why-did-you-need-to-get-someones-chrome-cookies-in-the-first-place)
