<?php

namespace App;

require __DIR__ . '/app/templates/login.php';
require __DIR__ . '/app/templates/main.php';
require __DIR__ . '/app/controller.php';

use \App\Templates as Templates;

function route () {
    session_start();

    /* Just outright abort if the user isn't logged in. */
    if (!$_SESSION['loggedin']) {
        Templates\loginTemplate(array());
        return;
    }


    /* Otherwise if the user is logged in, send them to the actual app. */
    $dbPath = __DIR__ . '/db/main.db';
    $controller = new \App\Controller($dbPath);
    $user = $controller->getUser($_SESSION['userid']);

    Templates\mainTemplate(array(
        'username' => $user[0],

        //Get list of notes automatically?
        //Nah... I want to be able to re-render the entire state where necessary.
    ));
}

route();

/*
 * This is going to be a single page app.
 *
 * Which means in an Apache deployment, this
 * should be the only HTML entry point.
 *
 * If the user doesn't have a session set,
 * give them a login page. Otherwise, give
 * them an app edit.
 *
 * I'll need to expose a few other endpoints as
 * POST requests... Should they be separate URLs?
 * I'm not sure. I think yes, because otherwise
 * adding data is going to be problematic?
 *
 * And I want to be able to...
 */

?>
