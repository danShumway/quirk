(function () {
    'use strict';

    var logout = document.querySelector('.logout__logout');

    logout.addEventListener('click', function (e) {
        fetch('/rest/logout.php', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.json();
        }).then(function (response) {
            if (response.result) {
                window.location.reload(true);
                return;
            }

            throw new Error('Request failed');
        }).catch(function (err) {
            //Display error on screen.
            //I have no idea what a logout error would look like
            //But I guess I can catch one.
        });
    });
}());
