(function () {
    'use strict';

    var login = document.querySelector('.login__login');
    var signup = document.querySelector('.login__signup');

    function setup (form) {

        form.addEventListener('submit', function (e) {
            e.preventDefault();

            var data = (function () {
                var formData = new FormData(form);

                var data = {};
                formData.forEach(function (value, key) {
                    data[key] = value;
                });

                return data;
            }());

            fetch(form.action, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            }).then(function (response) {
                return response.json();
            }).then(function (response) {
                if (response.result) {
                    window.location.reload(true);
                    return;
                }

                throw new Error('Request failed');
            }).catch(function (err) {
                //Display error on screen.
            });

            return false;
        });
    }

    setup(document.querySelector('.login__login'));
    setup(document.querySelector('.login__signup'));
}());
